console.log("Hello World")

// Solution:

function addTwoNum(num1,num2){
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}

addTwoNum(5,15);

function subtractTwoNum(num1,num2){
	let difference = num1 + num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);
}

subtractTwoNum(20,5);

function multiplyTwoNum(num1,num2){
	let product1 = num1 * num2;
	console.log("The product of " + num1 + " and " + num2 + ":");
	return product1;
}

let product = multiplyTwoNum(50,10);
console.log(product);

function divideTwoNum(num1,num2){
	let quotient1 = num1 / num2;
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return quotient1;
}

let quotient = divideTwoNum(50,10);
console.log(quotient);

function computeAreaOfCircle(radius){
	let pi = 3.14;
	let area = pi * (radius ** 2);
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return area;
}

let circleArea = computeAreaOfCircle(15);
console.log(circleArea);

function averageOfScores(num1,num2,num3,num4){
	let average = (num1 + num2 + num3 + num4) / 4;
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", and " + num4);
	return average;
}

let averageVar = averageOfScores(20,40,60,80);
console.log(averageVar);

function computeGrade(score,totalScore){
	console.log("Is " + score + "/" + totalScore + " a passing score?");
	let percentage = (score / totalScore) * 100;
	let isPassed = percentage >= 75;
	return isPassed;
}

let grade = computeGrade(38,50);
console.log(grade);
